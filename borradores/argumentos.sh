#!/bin/bash
#set -x

# argumentos.sh <hostip> <pings> <protocolo> <broadcast>

hostip=$1
pings=$2
protocolo=$3
bcast="$4"

# -4 IPv4
# -6 IPv6
# -b broadcast
# -c cantidad de pings

# verificar que la variable "$protocolo" tenga contenido.

test -n $protocolo || exit

# testear que "$protocolo" sea un 4 o un 6

test $protocolo -eq 4 -o 6 || exit

test $bcast = "-b" || exit

gateway=$(ip route | grep "default" | cut -d " " -f 3,5 | tr " " ":")

echo "GATEWAY"
echo $(echo $gateway | cut -d ":" -f 1)
echo $(echo $gateway | cut -d ":" -f 2)

ping -c $pings -$protocolo $bcast $hostip  1> /dev/null 2>&1 && echo "Se alcanzó el destino $hostip" || echo "No se pudo alcanzar el destino $hostip"


