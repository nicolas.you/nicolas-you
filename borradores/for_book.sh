#!/bin/bash
 

 
BOOKS=('In Search of Lost Time' 'Don Quixote' 'Ulysses' 'The Great Gatsby')
 
for book in "${BOOKS[@]}"; do

	case $book in
		${BOOKS[1]})
			echo "'$book' Agotado"
			sleep 0.5
			;;
		*)
			echo "'$book' Hay stock"
			sleep 0.5
			;;
	esac	

done


