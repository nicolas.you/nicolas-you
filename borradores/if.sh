#!/bin/bash





# evaluamos la existencia del usuario en el fichero '/etc/passwd'
# almacenamos el resultado en una variable.

if user=$(grep -w "$1" /etc/passwd) ; then
	echo "Se ha encontrado el usuario $1"	
	echo $user
else
	echo "No se ha encontrado el usuario $1"
	# El script se sale
	# ya que el usuario de entrada no existe
	exit
fi

# Evaluar si el usuario ingresado corresponde a:
#	- Usuario 'root' (id 0)


#Array conlos niveles de usuario
usrlvl=( "root" "especial sistema" "usuario estandar" )

# Aislar el UID
user=$(echo $user | cut -d ":" -f 3)

# Comparación del UID

if [ $user -eq 0 ]; then
	echo "El usuario es ${usrlvl[0]}"
# Se evalúa un rango entre 1 y 499
# ambas comparaciones tienen que devolver true para
# que la condición sea 'true'
elif [ $user -ge 1 ] && [ $user -le 499 ]; then
	echo "El usuario es ${usrlvl[1]}"
else
	echo "El usuario es ${usrlvl[2]}"
fi
