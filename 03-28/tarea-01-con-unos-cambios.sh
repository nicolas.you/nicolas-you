#!/bin/bash

# Este script es similar al 1ro, utiliza las mismas variables, algunas están en otro orden.

# Se utiliza el comando sleep para generar un poco de delay.
sleep 2

echo "Se han realizado unos cambios."

sleep 1

echo "Reejecutando el script."

sleep 2

# Se invoca al comando 'echo' para mostrar mensajes acerca de la TAREA-01

echo "Bienvenido Prof. $ESTUDIANTE."
echo ""
echo "Esta es la $TAREA del estudiante:"
echo "$PROFESOR"
echo ""
echo "La fecha de entrega de la $TAREA es el:"
echo "$FECHAENTREGA."
echo ""
echo "La fecha de hoy es:"

# Se invoca al comando 'date' para mostrar la fecha del momento de ejecución del script
date
echo ""
echo "Si está viendo esto, significa que la $TAREA de $PROFESOR se entregó en tiempo y forma."

#EOF
