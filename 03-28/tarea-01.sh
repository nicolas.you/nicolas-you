#!/bin/bash

# En este scrip se verán aplicados los conocimientos que adquirí en las 1ras dos clases de la materia Programación

# Se declaran las variables y se exportan para ser utilizadas en el 2do script.


export PROFESOR="Sergio Pernas"
export ESTUDIANTE="Nicolás You"
export TAREA="TAREA-01 03-28"
export FECHAENTREGA="lun 28 de marzo 2022 a las 19:00"

# Se invoca al comando 'echo' para mostrar mensajes acerca de la TAREA-01 

echo "Bienvenido Prof. $PROFESOR."
echo ""
echo "Esta es la $TAREA del estudiante:"
echo "$ESTUDIANTE"
echo ""
echo "La fecha de entrega de la $TAREA es el:"
echo "$FECHAENTREGA."
echo ""
echo "La fecha de hoy es:"

# Se invoca al comando 'date' para mostrar la fecha del momento de ejecución del script
date
echo ""
echo "Si está viendo esto, significa que la $TAREA de $ESTUDIANTE se entregó en tiempo y forma."

# Se ejecuta el 2do script

./tarea-01-con-unos-cambios.sh

#EOF
