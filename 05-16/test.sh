#!/bin/bash

args=( $@ )

#validacion de argumentos

#if [[ $# -lt 2 ]]; then

#	echo "Ingrese al menos 1 opción y el <host/ip>."
#	exit 1
#fi

for opcion in "${!args[@]}"; do

        case ${args[$opcion]} in
		
		--help)
                        echo "Uso: $0 [-C count] [-p protocolo <4> o <6>] [-T timestamp] [-b broadcast] <destino> "
			exit 1
		;;

                -C)
                        if ! [[ ${args[$((opcion+1))]} -gt 0 ]] ; then

                                echo "El arguemnto para la opcion '-C' tiene que ser un entero positvo."
				exit 1	
			fi
			
		;;

		-p)
			if [[ ${args[$((opcion+1))]} -ne 4 ]] && [[ ${args[$((opcion+1))]} -ne 6 ]] ; then
				
				echo "El argumento para la opcion '-p' debe ser 4 o 6."
				exit 1
			fi
		;;

		${args[-1]}) # Validación para el último argumento
		
			if [[ $# -le 2 ]] && [[ "${#args[-1]}" -lt 4 ]]; then

			        echo "Ingrese al menos 1 opción y el <host/ip>."
        			exit 1
			fi


			if ! [[ "${args[-1]}" =~ ([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)+[a-zA-Z]{2,} ]] && ! [[ "${args[-1]}" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$ ]]; then
				# Valida con 2 regex
				# 1ro: caracteres permitidos en un hostname o dominio
				# 2do: caracteres permitidos para un ip address. 250 a 255 | 200 a 249 | 0 a 199 seguido por un '.' 3 veces y 1 vez sin '.'
				echo "Ingrese un hostame, dominio o direccion ip válido."
				exit 1
			fi
			
		;;

		[^"-C""-T""-p""-b"])
			echo "Argumento inválido. Ejecute el script con '--help' para ver la ayuda."
			exit 1
		;;
	esac
done	

for opcion in "${!args[@]}"; do

        case ${args[$opcion]} in

                -C)
                        cantidad=${args[$(($opcion+1))]}
                        counter="-c $cantidad"
                        ;;

                -T)
                        timestamp="-D"
                        ;;

                -p)
                        proto=${args[$(($opcion+1))]}
                        p="-$proto"
                        ;;

                -b)
                        b="-b"
                        ;;

        esac
done

ping $b $timestamp $p $counter ${args[-1]}

