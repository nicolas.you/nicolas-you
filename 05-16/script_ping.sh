#!/bin/bash

args=( $@ )

#validacion de argumentos

for opcion in "${!args[@]}"; do

        case ${args[$opcion]} in

                -C)
                        if ! [[ ${args[$((opcion+1))]} -gt 0 ]]; then

                                echo "El argumento para la opcion '-C' tiene que ser un entero positvo."
                                exit 1
                        fi

                ;;

		-p)
                        if [[ ${args[$((opcion+1))]} -ne 4 ]] && [[ ${args[$((opcion+1))]} -ne 6 ]]; then

                                echo "El argumento para la opcion '-p' debe ser 4 o 6"
                                exit 1
                        fi
                ;;

        esac
done

for opcion in "${!args[@]}"; do

	case ${args[$opcion]} in

		-C)
			cantidad=${args[$(($opcion+1))]}
			counter="-c $cantidad"
			;;

		-T)
		 	timestamp="-D"
			;;
		
		-p) 
			proto=${args[$(($opcion+1))]}
			p="-$proto"
			;;

		-b)
			b="-b"
			;;
	
	esac
done

ping $b $timestamp $p $counter ${args[-1]}
