#!/bin/bash

#Script para hacer backups con el comando 'rsync'

#Trae las variables declaradas en el archivo de configuracion
source rsync_backup.config

rsync ${RSYNC_OPCIONES[@]} $DIR_SOURCE $REMOTE_USER@$REMOTE_HOST:$RUTA_BACKUP 

