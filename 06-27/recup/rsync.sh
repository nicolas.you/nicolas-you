#!/bin/bash

#Script para realizar backups con el comando 'rsync'

#Trae las funciones de siguiente archivos.
source help.sh
source funciones.sh
source crontab.conf

#Recoge los argumentos en el array 'args'
args=( $@ )

#Evalúa si se pasaron argumentos al script.
if [[ ${args:-null} == "null" ]]; then #expansión del array a 'null'
	
        echo -e "Faltan argumentos. Modo de uso:\n$0 <opciones rsync> <SOURCE> <DESTINO>\nUtilzar la opción --help para ver la ayuda."
        exit 1

elif [ $# -lt 3 ] && [[ "${args[0]}" != "--help" ]]; then #Cantidad de elementos menor a 3 y no es '--help'

	echo -e "Faltan argumentos. Modo de uso:\n$0 <opciones rsync> <SOURCE> <DESTINO>\nUtilzar la opción --help para ver la ayuda."
        exit 1
 
elif [[ ${args[0]} == "--help" ]]; then

	func_help #llama la 'func_help' que está en help.sh
	exit 0

fi

for ind in "${!args[@]}"; do # ${!args[@]} -> para cada índice del array

	case ${args[$ind]} in
		
		ssh) 
			func_ssh ${args[@]}
			;;
		-*)
			func_opt ${args[$ind]}
			;;
		${args[-2]})
			func_fuente ${args[-2]}
			;;
		${args[-1]})
			func_dest ${args[-1]}
			;;
		cron)
			echo "$CRONTAB $0 $opt $fuente $dest" >> /var/spool/cron/$USER
			;;
	esac

done

rsync $opt $fuente $dest
