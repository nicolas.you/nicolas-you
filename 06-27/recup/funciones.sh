func_ssh () {
	
#Validación de argumentos para backup a remoto

	local remote=$(echo "${args[-1]}" | cut -d ":" -f 1)
	local host=$(echo "$remote" | cut -d "@" -f 2)
	local dest_remote=$(echo "${args[-1]}" | cut -d ":" -f 2)
	
	if ! ssh -V > /dev/null 2>&1 ;then #Ejecuta ssh con opción de ver la versión y redirige la salida a /dev/null. Si el exit status != 0, ssh no está instalado.
		echo "No cuenta con el servicio 'ssh' instalado. Por favor instalarlo para ejecutar el script."
		exit 1
	elif ! ping -c 1 "$host" > /dev/null 2>&1 ;then #Evalúa la conexion con host
		
	  	echo "No se puede establecer una conexión con el host '$host'. Verifique que esté bien escrito o la conectividad de la red."
		exit 1	
	#elif ! ssh $host "test -d $dest_remote" ;then #Verifica que el directorio del server remoto exista
		
	#	echo "El directorio '$dest_remote' en '$host' no existe."
	#	exit 1
	fi	

################################

#Control de llaves. Quise hacer una función aparte pero se utiliza la variable 'remote' de nuevo.

	local key=${args[$(($ind+1))]} #Define variable 'KEY' con el argumento que le sigue a ssh.
	if [[ "$key" =~ "no-key" ]]; then #evalúa si es 'no-key' => genera llave y envía al host.

		ssh-keygen -t rsa
		ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@$remote

	elif [[ "$KEY" =~ "yes-key" ]];then #evalúa si es 'yes-key' => envía la llave al host.
		
		ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@$remote

	fi
}

func_opt () {
	
	opt=$(echo "${args[$ind]}" | cut -d "-" -f 2)
        if ! [[ "$opt" =~ ([a-zA-Z0-9]) ]]; then
        	echo "Utilice una opción de 'rsync' válida."
                exit 1
        else
        	opt="${args[$ind]}"
        fi

}

func_fuente () {
	fuente="${args[-2]}"
	if ! [ -d "$fuente" ] ;then
		echo "El directorio FUENTE '$fuente' no existe."
		exit 1
	else 
		fuente=$fuente
	fi
}

func_dest () {
	dest="${args[-1]}"
	timestamp="$(date '+%Y-%m-%d_%H%M%S')"
	dest="$dest.$timestamp"

}

