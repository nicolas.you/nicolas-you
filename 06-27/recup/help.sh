#!/bin/bash

func_help () {
	
	echo -e "Ayuda de $0\nScript para realizar backups con 'rsync'\nModo de uso: $0 OPCIONES FUENTE DESTINO\nOpciones válidas:"
	echo -e "-avz\t\tOpciones propias de 'rsync'. Ver el manual de rsync."
	echo -e "ssh yes-key\tBackup a host remoto. Ya posee llave de autenticación."
	echo -e "ssh no-key\tBackup a host remoto. No posee llave de autenticación y desea crear una."
	echo -e "cron\t\tAgregar tarea al crontab."
	echo -e "--help\tImprime esta ayuda y finaliza."
}

