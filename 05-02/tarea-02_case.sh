#!/bin/bash

FILE=$1

case "$(sudo test -e $FILE)" in
	0)
		echo "El fichero $FILE SI existe"
		;;
	1)
		echo "El fichero $FILE NO existe"
		;;
	*)
		exit 1
esac

