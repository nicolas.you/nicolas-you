#!/bin/bash

# tarea-02.sh <fichero>

FILE=$1

# Se evalua si exite el fichero
# Se utiliza sudo para los archivos requieran permisos

if sudo test -e $FILE ; then

	echo "El fichero '$FILE' SI existe"
	
	# Se evalua si el usuario tiene permisos de ejecución

	if test -x $FILE ; then

		echo "El usuario '$USER' SI tiene permisos de ejecución"
		ls -l $FILE
		
		# Se evalua si quiere ejecutar el archivo
		read -p "¿Desea ejecutar '$FILE'? [Y/n]" ANS

		if [[ "$ANS" =~ [Yy] ]]; then

			echo "Ejecutando '$FILE'"
		elif [[ "$ANS" =~ [Nn] ]]; then

			echo "No se ejecutó '$FILE'"
			exit 1
			#PREGUNTARLE AL PROFE si se pone exit 1
		else

			echo "Responda con [Y/n]"
			exit 1
		fi
	else

		echo "EL usuario '$USER' NO tiene permisos de ejecución"
		
		# Se evalua si el usuario es dueño del fichero
		
		if test -O $FILE; then

			read -p "¿Desea otorgar permisos de ejecución a '$FILE'? [Y/n]" ANS
			if [[ "$ANS"  =~ [Yy] ]]; then
			
				chmod +x $FILE
				ls -l $FILE
			elif [[ "$ANS" =~ [Nn] ]]; then
			
				echo "No se han otorgado permisos de ejecución a '$FILE'"
			else
	
				echo "Responda con [Y/n]"
				exit 1
			fi
		fi

	fi

else

	echo "El fichero '$FILE' NO existe"
	read -p "¿Desea crearlo? [Y/n]" ANS
	if [[ "$ANS" =~ [Yy] ]]; then 

		touch $FILE
	elif [[ "$ANS" =~ [Nn] ]]; then
		
		echo "No se ha creado '$FILE'"
	else 
		echo "Responda con [Y/n]"
		exit 1
	fi
fi

