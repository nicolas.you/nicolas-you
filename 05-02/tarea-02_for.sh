#!/bin/bash

# tarea-02.sh <fichero1> <fichero2> ... <ficheroN>

FILE=( $@ )

for i in ${FILE[@]}; do

  if sudo test -e $i ; then

	echo "El fichero '$i' SI existe"
	
	# Se evalua si el usuario tiene permisos de ejecución

	if test -x $i ; then

		echo "El usuario '$USER' SI tiene permisos de ejecución"
		ls -l $i
		
		# Se evalua si quiere ejecutar el archivo
		read -p "¿Desea ejecutar '$i'? [Y/n]" ANS

		if [[ "$ANS" =~ [Yy] ]]; then

			echo "Ejecutando '$i'"
		elif [[ "$ANS" =~ [Nn] ]]; then

			echo "No se ejecutó '$i'"
			exit 1
			#PREGUNTARLE AL PROFE si se pone exit 1
		else

			echo "Responda con [Y/n]"
			exit 1
		fi
	else

		echo "EL usuario '$USER' NO tiene permisos de ejecución"
		
		# Se evalua si el usuario es dueño del fichero
		
		if test -O $i; then

			read -p "¿Desea otorgar permisos de ejecución a '$i'? [Y/n]" ANS
			if [[ "$ANS"  =~ [Yy] ]]; then
			
				chmod +x $i
				ls -l $i
			elif [[ "$ANS" =~ [Nn] ]]; then
			
				echo "No se han otorgado permisos de ejecución a '$i'"
			else
	
				echo "Responda con [Y/n]"
				exit 1
			fi
		fi

	fi

  else
	
	echo "El fichero '$i' NO existe"
       	
	# Evaluar si se quiere crear el fichero
       	read -p "¿Desea crearlo? [Y/n]" ANS
        if [[ "$ANS" =~ [Yy] ]]; then

                touch $i
        elif [[ "$ANS" =~ [Nn] ]]; then

                echo "No se ha creado '$i'"
        else
                echo "Responda con [Y/n]"
                exit 1
        fi
  fi
		
done
