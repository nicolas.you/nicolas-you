#!/bin/bash

func_help () {

        local arg="$1"

        echo "Modo de uso: $0 <opción> ... <destino>"
        echo "Recoge las opciones para el comando 'ping'."
        echo ""
        echo "Pasar como argumento al menos 1 <opción> y el <destino>."
        echo "Opciones válidas:
        -C      Count. Seguido por un entero positivo, fija la cantidad de veces a pingear.
        -T      Timestamp. Imprime el tiempo antes de cada línea.
        -p      Protocol. Debe estar seguido por un '4' o '6'. Utiliza el protocolo IPv4 o IPv6.
        -b      Broadcast. Permite pingear a una dirección broadcast.
        --help  Imprime esta ayuda y finaliza."

        # Se evalúa la salida de la función. Si se llama a la ayuda por una orden, sale con status 0.
        # Si es por error, sale con 1.
        if [ $arg -eq 0 ]; then
                exit 0
        else
                exit 1
        fi
}
