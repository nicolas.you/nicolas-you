#!/bin/bash

args=( $@ )

if [[ ${args:-null} == "null" ]]; then
	echo "Ingrese por lo menos 1 opción y el <destino> a pingear."
	exit 1
fi

if [ $# -lt 2 ] && [[ "${args[-1]}" != "--help" ]]; then
        echo "Ingrese por lo menos 1 opción y el <destino> a pingear."
        exit 1
fi



# For loop para recoger y validar los argumentos pasados al script
for opcion in "${!args[@]}"; do # ${!args[@]} significa para cada índice

	case ${args[$opcion]} in
		
		--help)
			echo "Modo de uso: $0 <opción> ... <destino>"
			echo "Recoge las opciones para el comando 'ping'."
			echo ""
			echo "Pasar como argumento al menos 1 <opción> y el <destino>."
			echo "Opciones válidas:
			-C	Count. Seguido por un entero positivo, fija la cantidad de veces a pingear.
			-T	Timestamp. Imprime el tiempo antes de cada línea.
			-p	Protocol. Debe estar seguido por un '4' o '6'. Utiliza el protocolo IPv4 o IPv6.
			-b	Broadcast. Permite pingear a una dirección broadcast.
			--help	Imprime esta ayuda y finaliza."
			exit 1
			;;
		-C) #Para la opcion '-C', recoge el argumento que le sigue y evalúa si es un entero positivo por medio de una expresión regular. 
		     	CANTIDAD=${args[$(($opcion+1))]}
			if [[ "$CANTIDAD" =~ ^[1-9]+[0-9]*$ ]]; then #Regex -> empieza con 1 al 9 y puede seguirle cualquier otro num (o no)
				COUNTER="-c $CANTIDAD"
			else
				echo "El argumento para la opción '-C' debe ser un entero positivo."
				exit 1
			fi
			;;
		-T)
			TIMESTAMP="-D"
			;;
		-p) # Opción '-p', recoge el argumento que le sigue y evalúa si la cadena es un '4' o '6'.
			PROTO=${args[$((opcion+1))]}
			if [[ "$PROTO" == "4" || "$PROTO" == "6" ]]; then
				P="-$PROTO"
			else
				echo "El argumento para la opción '-p' debe ser '4' o '6'."
				exit 1
			fi
			;;
		-b)
			B="-b"
			;;
		${args[-1]}) # Validación para el último argumento (debe ser un destino válido).
			if ! [[ "${args[-1]}" =~ ([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)+[a-zA-Z]{2,} ]] && ! [[ "${args[-1]}" =~ ^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$ ]]; then
                                # Valida con 2 regex
                                # 1ro: caracteres permitidos en un hostname o dominio
                                # 2do: caracteres permitidos para un ip address. 250 a 255 | 200 a 249 | 0 a 199 seguido por un '.' 3 veces y 1 vez sin '.'
                                echo "Ingrese un hostame, dominio o direccion ip válido."
                                exit 1
                        fi
			;;
		*) #Todos los demás argumentos arroja error.
			echo "Argumento inválido. Ejecute el script con la opción '--help' para ver la ayuda."
			exit 1
			;;
	esac
done

ping $B $TIMESTAMP $P $COUNTER ${args[-1]}
