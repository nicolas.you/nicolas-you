#!/bin/bash

args=( $@ )

source func_help.sh
source func_validation.sh

func_validation $@

ping $B $TIMESTAMP $P $COUNTER ${args[-1]}
